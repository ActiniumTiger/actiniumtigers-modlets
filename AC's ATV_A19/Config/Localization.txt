Key,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish

	<!-- 
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
									        AC'S ATV										
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -->

	<!-- ###### AC'S ATV ###### -->
	AC_ATV,vehicles,vehicle,new,ATV,,,,,
	AC_ATVDesc,vehicles,vehicle,new,Add's an ATV to game,,,,,
	AC_ATV_Chassis,items,Part,EnChanged,ATV Chassis,,,,,
	AC_ATV_ChassisDesc,items,Part,EnChanged,The ATV Chassis is needed to craft the ATV. The beginning of something...awesome!,,,,,
	AC_ATV_ChassisSchematic,items,Part,EnChanged,ATV Chassis Schematic,,,,,
	AC_ATV_ChassisSchematicDesc,items,Part,EnChanged, Schematic needed to craft the Chassis for the ATV. The beginning of something...awesome!,,,,,
	AC_ATV_Body,items,Part,EnChanged,ATV Body,,,,,
	AC_ATV_BodyDesc,items,Part,EnChanged,The ATV Body is needed to craft the ATV. The beginning of something...awesome!,,,,,
	AC_ATV_BodySchematic,items,Part,EnChanged,ATV Body Schematic,,,,,
	AC_ATV_BodySchematicDesc,items,Part,EnChanged,Schematic needed to craft the ATV. The beginning of something...awesome!,,,,,
	AC_ATVplaceable,vehicles,item,EnChanged,ATV,,,,,
	AC_ATVplaceableDesc,vehicles,item,EnChanged,A custom ATV.,,,,,
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
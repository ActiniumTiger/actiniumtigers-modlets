Key,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish

<!-- 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
									AC'S & RAGSY'S MI-17                                                                                           
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-->

--------------------Items----------------------
vehicleMi17Schematic,"[e7a729]Schematic: Helicopter[-]","[e7a729]Japanese[-]"
vehicleMi17Chassis,items,Part,EnChanged,AC_Mi_17 Chassis,,,,,
vehicleMi17ChassisDesc,items,Part,EnChanged,Parts needed to craft the AC_Mi_17. Grease Monkey level 5 required.,,,,,
vehicleMi17Rotors,items,Part,EnChanged,AC_Mi_17 Rotors,,,,,
vehicleMi17RotorsDesc,items,Part,EnChanged,Parts needed to craft the AC_Mi_17.,,,,,
vehicleMi17,vehicles,vehicle,new,AC_Mi_17,,,,,
vehicleMi17Placeable,vehicles,item,EnChanged,AC_Mi_17,,,,,
vehicleMi17PlaceableDesc,vehicles,item,EnChanged,Up Up and Away!!,,,,,
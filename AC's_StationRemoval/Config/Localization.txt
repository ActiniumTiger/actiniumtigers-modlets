Key,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish
<!-- 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
										WORKSTATION REMOVAL                                                                                            
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-->
meleeToolWorkstationRemovalToolAC,items,Tools,New,"Workstation Removal Tool",,,,,
meleeToolWorkstationRemovalToolACDesc,items,Tools,New,"This tool will allow you to pick up any functional workstations in the world. To use it, equip on your hotbar and right-click on the workstation whilst having a workstation removal kit in your backpack.",,,,,

meleeToolWorkstationRemovalToolACSchematic,items,Schematic,New,"Workstation Removal Tool Schematic",,,,,

meleeToolForgeRemovalToolAC,items,Tools,New,"Forge Removal Tool",,,,,
meleeToolForgeRemovalToolACDesc,items,Tools,New,"This tool will allow you to pick up any functional Forges in the world. To use it, equip on your hotbar and right-click on the Forge whilst having a Forge removal kit in your backpack.",,,,,

meleeToolForgeRemovalToolACSchematic,items,Schematic,New,"Forge Removal Tool Schematic",,,,,

meleeToolChemStationRemovalToolAC,items,Tools,New,"Chem Station Removal Tool",,,,,
meleeToolChemStationRemovalToolACDesc,items,Tools,New,"This tool will allow you to pick up any functional Chemistry Station in the world. To use it, equip on your hotbar and right-click on the Chemistry Station whilst having a Chemistry Station removal kit in your backpack.",,,,,

meleeToolChemStationRemovalToolACSchematic,items,Schematic,New,"Chem Station Removal Tool Schematic",,,,,

meleeToolMixerRemovalToolAC,items,Tools,New,"Mixer Removal Tool",,,,,
meleeToolMixerRemovalToolACDesc,items,Tools,New,"This tool will allow you to pick up any functional Cement Mixer in the world. To use it, equip on your hotbar and right-click on the Cement Mixer whilst having a Mixer removal kit in your backpack.",,,,,

meleeToolMixerRemovalToolACSchematic,items,Schematic,New,"Mixer Removal Tool Schematic",,,,,

resourceWorkstationRemovalKitAC,items,Resources,New,"Workstation Removal Kit",,,,,
resourceWorkstationRemovalKitACDesc,items,Resources,New,"This will allow you to remove any workstation in the world and then pick it up. Right-click on the workstation using a workstation removal tool whilst this is in your backpack.",,,,,

resourceWorkstationRemovalKitACSchematic,items,Schematic,New,"Workstation Removal Kit Schematic",,,,,

resourceForgeRemovalKitAC,items,Resources,New,"Forge Removal Kit",,,,,
resourceForgeRemovalKitACDesc,items,Resources,New,"This will allow you to remove any Forge in the world and then pick it up. Right-click on the Forge using a Forge removal tool whilst this is in your backpack.",,,,,

resourceForgeRemovalKitACSchematic,items,Schematic,New,"Forge Removal Kit Schematic",,,,,

resourceChemStationRemovalKitAC,items,Resources,New,"Chem Station Removal Kit",,,,,
resourceChemStationRemovalKitACDesc,items,Resources,New,"This will allow you to remove any Chemistry Station in the world and then pick it up. Right-click on the Chemistry Station using a Chemistry Station removal tool whilst this is in your backpack.",,,,,

resourceChemStationRemovalKitACSchematic,items,Schematic,New,"Chemistry Station Removal Kit Schematic",,,,,

resourceMixerRemovalKitAC,items,Resources,New,"Cement Mixer Removal Kit",,,,,
resourceMixerRemovalKitACDesc,items,Resources,New,"This will allow you to remove any Cement Mixer in the world and then pick it up. Right-click on the Cement Mixer using a Cement Mixer removal tool whilst this is in your backpack.",,,,,

resourceMixerRemovalKitACSchematic,items,Schematic,New,"Cement Mixer Removal Kit Schematic",,,,,

resourceWorkbenchPartsAC,items,Resources,New,"Salvaged Workbench Parts",,,,,
resourceForgePartsAC,items,Resources,New,"Salvaged Forge Parts",,,,,
resourceChemStationPartsAC,items,Resources,New,"Salvaged ChemStation Parts",,,,,
resourceMixerPartsAC,items,Resources,New,"Salvaged Mixer Parts",,,,,
salvagedPartsGroupDesc,items,Resources,New,"Salvaged workstation parts can be used to upgrade any destroyed workstation of the same name into a fully working one.",,,,,

cntWorkbenchBoxAC,blocks,Container,New,"Boxed Workbench",,,,,
cntWorkbenchBoxACDesc,blocks,Container,New,"This is a workbench in a box. You can pick this up to retrieve a workbench, or destroy the box to pick up the workbench.",,,,,
cntForgeBoxAC,blocks,Container,New,"Boxed Forge",,,,,
cntForgeBoxACDesc,blocks,Container,New,"This is a forge in a box. You can pick this up to retrieve a forge, or destroy the box to pick up the forge.",,,,,
cntChemStationBoxAC,blocks,Container,New,"Boxed Chemistry Station",,,,,
cntChemStationBoxACDesc,blocks,Container,New,"This is a chemistry station in a box. You can pick this up to retrieve a chemistry station, or destroy the box to pick up the chemistry station.",,,,,
cntMixerBoxAC,blocks,Container,New,"Boxed Cement Mixer",,,,,
cntMixerBoxACDesc,blocks,Container,New,"This is a cement mixer in a box. You can pick this up to retrieve a cement mixer, or destroy the box to pick up the cement mixer.",,,,,
cntTableSawBoxAC,blocks,Container,New,"Boxed Table Saw",,,,,
cntTableSawBoxACDesc,blocks,Container,New,"This is a table saw in a box. You can pick this up to retrieve a table saw, or destroy the box to pick up the table saw.",,,,,

perkSalvageOperationsRank3LongDescAC,progression,Perk,New,"You seem to have a knack for salvaging more useful parts than the average scrapper. Craft fair quality wrenches. Deal 30% more damage, harvest 60% faster and gains 60% more resources when salvaging with wrenches.
Unlocks crafting of Workstation Removal Tool and Workstation Removal Kit.",,,,,